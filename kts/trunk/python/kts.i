%module kts

%inline %{

#include <ktsdefs.h>
#include <kts.h>
#include <ktsds.h>

// ktsformat.c
extern void State2FilePrlg(FILE* stream, long oprtr);
extern void State2FilePcfg(FILE* stream, long oprtr);
extern int GetMorphTag( Path pathPool
                      , int i_th
                      , unsigned char* _mporph
                      , char* _tag
                      , char* eoj_space );
extern void DisplayEojeol(FILE* stream, Path pathPool, char separator);
extern void DisplayPath(FILE* stream, int* path);
extern void DisplayPathM(FILE* stream);
extern void DisplayState(FILE* stream, long oprtr);

// kts.c
extern long OpenKTS(long dummy);
extern long CloseKTS(long dummy);
extern long MAT( char* str
               , long op
               , double state_thr
               , double path_thr
               , long num_path);
extern void PutAnalysis();



%}
