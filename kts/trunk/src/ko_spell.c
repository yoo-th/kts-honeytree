// Copyright : Honeytree Co., Ltd. 
// License : GPL
// Author : yoo@honeytree.net

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>

static
const uint16_t choseong[] = { 0x3131		// G  KIYEOK
                            , 0x3132		// GG SSANGKIYEOK
                            , 0x3134		// N  NIEUN
                            , 0x3137		// D  TIKEUT/DIGUD
                            , 0x3138		// DD SSANGTIKEUT/SSANGDIGUD
                            , 0x3139		// R  RIEUL
                            , 0x3141		// M  MIEUM
                            , 0x3142		// B  PIEUP/BIEUB
                            , 0x3143		// BB SSANGPIEUP/SSANGBIEUB
                            , 0x3145		// S  SIOS
                            , 0x3146		// SS SSANGSIOS
                            , 0x3147		// *  IEUNG
                            , 0x3148		// Z  CIEUC(JIEUT)
                            , 0x3149		// ZZ SSANGCIEUC(SSANGJIEUT)
                            , 0x314a		// C  CHIEUCH
                            , 0x314b		// K  KHIEUKH
                            , 0x314c		// T  THIEUTH
                            , 0x314d		// P  PHIEUPH
    			    , 0x314e };		// H  HIEUH
 
static
const uint16_t jungseong[] = { 0x314f		// A
                             , 0x3150		// AE
                             , 0x3151		// YA 
                             , 0x3152		// YAE
                             , 0x3153		// EO
                             , 0x3154		// E
                             , 0x3155		// YEO
                             , 0x3156		// YE
                             , 0x3157		// O 
                             , 0x3158		// WA
                             , 0x3159		// WAE  WHY
                             , 0x315a		// OE   CHAMOE
                             , 0x315b		// YO 
                             , 0x315c		// U
                             , 0x315d		// WEO
                             , 0x315e		// WE
                             , 0x315f		// WI
                             , 0x3160		// YU
                             , 0x3161		// EU
                             , 0x3162		// YI
                             , 0x3163 };	// I

static
const uint16_t jongseong[] = { 0
 	                     , 0x3131		// g
                             , 0x3132		// gg
                             , 0x3133		// gs
                             , 0x3134		// n
                             , 0x3135		// nz
                             , 0x3136		// nh
                             , 0x3137		// d
                             , 0x3139		// r
                             , 0x313a		// rb
                             , 0x313b		// rm
                             , 0x313c		// rb
                             , 0x313d		// rs
                             , 0x313e		// rt
                             , 0x313f		// rp
                             , 0x3140		// rh
                             , 0x3141		// m
                             , 0x3142		// b
                             , 0x3144		// bs
                             , 0x3145		// s
                             , 0x3146		// ss
                             , 0x3147		// *
                             , 0x3148		// z
                             , 0x314a		// c
                             , 0x314b		// k
                             , 0x314c		// t
                             , 0x314d		// p
                             , 0x314e };		// h
 
// we only deal with contemporary hangul
static const int hangul_base = 0xAC00u;
static const int num_cho_charset = 588;
static const int num_jung_charset = 21;
static const int num_jong_charset = 28;

static const int hangul_ga   = 0xAC00u;
static const int hangul_last = 0xD7AFu;
static const int hangul_hih  = 0xD7A3u;
static const int hangul_kiyeok = 0x3131u;
static const int hangul_i    = 0x3163u;

#define is_eumjeol(c) (hangul_ga <= (c) && (c) <= hangul_hih)
#define is_jamo(c) (hangul_kiyeok <= (c) && (c) <= hangul_i)
#define is_hangul(c) (is_eumjeol(c) || is_jamo(c))

//http://warmz.tistory.com/717

//http://lapis.pe.kr/dokuwiki/charset/unicode/hangeul
//http://www.codeproject.com/Articles/17480/Optimizing-integer-divisions-with-Multiply-Shift-i
inline static int div28(int n, int* m)
{
	int rv = (n*9363)>>18;
	if (m) *m = n-rv*28;
	return rv;
}

inline static int div21(int n, int* m)
{
	int rv = (n*391)>>13;
	if (m) *m = n-rv*21;
	return rv;
}

bool split_han(int c, int* cho, int* jung, int* jong)
{
	if (!is_eumjeol(c)) return false; 
	c -= hangul_base;
	int m;
	c = div28(c, &m);

	if (jong) *jong = jongseong[m]; 
	c = div21(c, &m);

	if (jung) *jung = jungseong[m];
	if (cho) *cho = choseong[c];

	return true;
}

// N.B. machine endianess assumed
// http://helloworld.naver.com/helloworld/19187/
// http://www.lemoda.net/c/utf8-to-ucs2/index.html
int u8_u16(const char** s)
{
	int rv = -1;
	const char* c;

	if (!s || !*s) return -1;

	//fprintf(stderr, "%s\n", *s);

	c = *s;

	if (*c>0) {
		rv = *c++;
	} else switch (0xF0 & *c) {
	// 3byte utf8
	// 1110 xxxx 10xx xxxx 10xx xxxx
	case 0xE0:
		if ((c[1]&0xC0) != 0x80 || (c[2]&0xC0) != 0x80) break;
		rv = ((c[0]&0x0F)<<12) | ((c[1]&0x3F)<<6) | (c[2]&0x3F);
		c += 3;
		break;
	// 2byte utf8
	// 110x xxxx 10xx xxxx
	case 0xC0: case 0xD0:
		if ((c[1]&0xC0) != 0x80) break;
		rv = ((c[0]&0x1F)<<6) | (c[1]&0x3F);
		c += 2;
		break;
		// 4byte utf8
	// 4byte utf8
	// 1111 xxxx 10xx xxxx 10xx xxxx 10xx xxxx
	case 0xF0:
		// not implemented;
	default:
		break;
		return -1; // 0xFFFFFFFF
	}

	*s = c;
	return rv;
}

// N.B. machine endianess assumed
// http://www.lemoda.net/c/ucs2-to-utf8/index.html
// len of parameter out should be equal to or longer than 3
// retval: [1-3] length of converted utf8 code
int u16_u8(uint16_t in, char* out, const char* end)
{
	enum { INVALID_CHAR   = -1
	     , END_IS_NULL    = -2
	     , NOT_ENOUGH_BUF = -3 };

//	int rv = INVALID_CHAR; 

//	if ((unsigned)in < 0x80u) {		// 0x0000 ~ 0x007F 0x0080
//		rv = 1;
//	}
//	else if ((unsigned)in < 0x800u) {	// 0x0800 ~ 0x07FF 0x0800 
//		rv = 2;
//	}
//	else if ((unsigned)in < 0x10000u)  {	// else 0x0800 ~ 0xFFFF 0x10000
//		rv = 3;
//	}

	int rv = 1;

	if (in>>(3+8)) {
		rv = 3;
	}
	else if (in>>7) {
		rv = 2;
	}

	//if (rv<0 || !out) return rv;
	if (!out) return rv;
	if (!end) return END_IS_NULL;
	if (end-out<rv) return NOT_ENOUGH_BUF;

	switch (rv) {
	case 1:
		*out = in;
		break;
	case 2:
		*out++ = (in>>6) | 0xC0;
		*out = (in&0x3F) | 0x80;
		break;
	case 3:
		*out++ = (in>>12) | 0xE0;
		*out++ = ((in>>6)&0x3F) | 0x80;
		*out = (in&0x3F) | 0x80;
		break;
	default:
		// can not reach here
		break;
	}

	return rv;
}

size_t charlen_utf8(const char* s)
{
	if (!s) return 0;

	if (*s>=0) return 1;

	switch (0xF0 & *s) {
	case 0xE0: return 3;
	case 0xC0: case 0xD0: return 2;
	default: break;
	}

	return 0;
}

// count num of chars in utf8 
size_t strlen_utf8(const char* s)
{
	int len = 0;

	while (*s) {
		if (*s>0)  {
			++s;
		} else switch (0xF0 & *s) {
		case 0xE0: s += 3; break;
		case 0xC0: case 0xD0: s += 2; break;
				  
		// unexpected
		case 0xF0: s += 4;
		default:
			return 0;
		}
		++len;
	}

	return len;
}

// return output buffer size
int conv816(const char* s, uint16_t* d)
{
	uint16_t* beg = d;
	const char** ps;

	if (!s) return 0;

	ps = &s;

	if (!d) return (int)(strlen_utf8(s)<<1)+2;

	while (**ps) *d++ = u8_u16(ps);
	*d++ = 0;

	return d - beg;
}

// FIXME: check dest length
//static void conv168(const uint16_t* s, char* d)
//{
//	int c;
//
//	while ((c=*s++)) {
//		int len = u16_u8(c, d, d+4);
//		d += len;
//	}
//	*d = 0;
//}

static size_t utf8_len(const uint16_t* ucs2s)
{
	size_t len = 0;
	int c;
	while ((c=*ucs2s++)) {
		if (c>>(3+8)) {
			len += 3;
		}
		else if (c>>7) {
			len += 2;
		}
		else {
			++len;
		}
	}

	return len;
}

// return output buffer size
int conv168(const uint16_t* s, char* d)
{
	const char* beg = d;
	int c;

	if (!s) return 0;

	if (!d) return (int)utf8_len(s)+1;

	while ((c=*s++)) {
		int chlen = (c>>(3+8))? 3 : (c>>7)? 2 : 1;
		switch (chlen) {
		case 1:
			*d++ = (char)c;
			break;
		case 2:
			*d++ = (c>>6) | 0xC0;
			*d++ = (c&0x3F) | 0x80;
			break;
		case 3:
			*d++ = (c>>12) | 0xE0;
			*d++ = ((c>>6)&0x3F) | 0x80;
			*d++ = (c&0x3F) | 0x80;
			break;
		default: break;
		}
	}
	*d++ = 0;

	return d - beg;
}


