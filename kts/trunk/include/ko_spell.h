#ifndef __KO_SPELL_H__
#define __KO_SPELL_H__

#include <stdbool.h>
#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

// utf-8 -> utf-16
int u8_u16(const char** s);

// utf-16 -> utf-8
int u16_u8(uint16_t ucs2c, char* out, const char* end);

bool split_han(int ucs2c, int* cho, int* jung, int* zong);

size_t charlen_utf8(const char* s);

// count utf8 char
size_t strlen_utf8(const char* s);
int conv816(const char* s, uint16_t* d);

size_t strlen_utf16(const uint16_t* s);
int conv168(const uint16_t* s, char* d);


#ifdef __cplusplus
}
#endif

#endif

